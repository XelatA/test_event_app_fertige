class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Der Kommentar wurde gepostet!"
      redirect_to impression_url
    else
      @feed_items = []
      render 'static_pages/impression'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "Der Kommentar wurde erfolgreich entfernt!"
    redirect_to request.referrer || impression_url
  end

  private

  def micropost_params
    params.require(:micropost).permit(:content, :picture)
  end

  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to impression_url if @micropost.nil?
  end
end