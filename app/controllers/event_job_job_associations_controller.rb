class EventJobJobAssociationsController < ApplicationController
  before_action :set_event_job_job_association, only: [:show, :edit, :update, :destroy]

  # GET /event_job_job_associations
  # GET /event_job_job_associations.json
  def index
    @event_job_job_associations = EventJobJobAssociation.all
  end

  # GET /event_job_job_associations/1
  # GET /event_job_job_associations/1.json
  def show
  end

  # GET /event_job_job_associations/new
  def new
    @event_job_job_association = EventJobJobAssociation.new
  end

  # GET /event_job_job_associations/1/edit
  def edit
  end

  # POST /event_job_job_associations
  # POST /event_job_job_associations.json
  def create
    @event_job_job_association = EventJobJobAssociation.new(event_job_job_association_params)

    respond_to do |format|
      if @event_job_job_association.save
        format.html { redirect_to @event_job_job_association, notice: 'Die Vorgänger-Nachfolger-Beziehung wurde erfolgreich angelegt.' }
        format.json { render :show, status: :created, location: @event_job_job_association }
      else
        format.html { render :new }
        format.json { render json: @event_job_job_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_job_job_associations/1
  # PATCH/PUT /event_job_job_associations/1.json
  def update
    respond_to do |format|
      if @event_job_job_association.update(event_job_job_association_params)
        format.html { redirect_to @event_job_job_association, notice: 'Die Vorgänger-Nachfolger-Beziehung wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @event_job_job_association }
      else
        format.html { render :edit }
        format.json { render json: @event_job_job_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /event_job_job_associations/1
  # DELETE /event_job_job_associations/1.json
  def destroy
    @event_job_job_association.destroy
    respond_to do |format|
      format.html { redirect_to event_job_job_associations_url, notice: 'Die Vorgänger-Nachfolger-Beziehung wurde entfernt.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_job_job_association
      @event_job_job_association = EventJobJobAssociation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_job_job_association_params
      params.require(:event_job_job_association).permit(:event_id, :predecessor_id, :successor_id)
    end
end
