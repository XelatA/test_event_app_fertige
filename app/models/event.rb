class Event < ActiveRecord::Base
  has_many :jobs, dependent: :destroy
  has_many :periods, through: :event_job_period_associations
  has_many :event_job_period_associations, dependent: :destroy
  has_many :event_job_job_associations, dependent: :destroy
end
