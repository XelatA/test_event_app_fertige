require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links" do
    get root_path
    get signup_path
    get login_path

    assert_template 'sessions/new'
    assert_template 'layouts/_shim'
    assert_template 'layouts/_header'
    assert_template 'layouts/_footer'
    assert_template 'layouts/application'
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", signup_path
    assert_select "a[href=?]", impression_path
    assert_select "a[href=?]", impressum_path
    assert_select "a[href=?]", offer_path
    assert_select "a[href=?]", login_path
  end
end

