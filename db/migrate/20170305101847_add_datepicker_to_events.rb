class AddDatepickerToEvents < ActiveRecord::Migration
  def change
    add_column :events, :datepicker, :string
  end
end
